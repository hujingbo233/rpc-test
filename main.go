package main

import (
	"test/masterworker"
	"time"
)

func main() {
	m := masterworker.MakeMaster()
	go masterworker.Worker()
	for !m.Done() {
		time.Sleep(time.Second)
	}
}
