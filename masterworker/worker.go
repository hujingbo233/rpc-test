package masterworker

import (
	"fmt"
	"log"
	"net/rpc"
	"time"
)

func Worker() {
	empty := Empty{}
	task := TaskDescription{}
	for call("Master.AssignTask", &empty, &task) {
		if task.Type == NoTask {
			return
		}
		log.Print("worker: recieved task assignment", task)
		time.Sleep(time.Microsecond * 100)
		call("Master.FinishTask", &task, &empty)
	}
}

func call(rpcname string, args interface{}, reply interface{}) bool {
	sockname := masterSock()
	c, err := rpc.DialHTTP("unix", sockname)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer c.Close()

	err = c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
