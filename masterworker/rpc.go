package masterworker

type TaskType string

const (
	Task1 TaskType = "Task1"
	Task2          = "Task2"
)

type TaskStatus string

const (
	Idle   TaskStatus = "Idle"
	Done              = "Done"
	NoTask            = "NoTask"
)

type Empty struct{}

type TaskDescription struct {
	Type TaskType
	ID   int
}
