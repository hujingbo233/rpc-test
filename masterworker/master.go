package masterworker

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"sync"
)

type Master struct {
	sync.Mutex
	Stats1      []TaskStatus
	Stats2      []TaskStatus
	AllWorkDone bool
}

func masterSock() string {
	return "/var/tmp/golang-test-123321"
}

func (m *Master) server() {
	rpc.Register(m)
	rpc.HandleHTTP()
	os.Remove(masterSock())
	l, e := net.Listen("unix", masterSock())
	if e != nil {
		log.Fatal("master: fail to create,", e)
	}
	go http.Serve(l, nil)
}

func MakeMaster() *Master {
	m := Master{
		Stats1: make([]TaskStatus, 5),
		Stats2: make([]TaskStatus, 10),
	}
	for i := range m.Stats1 {
		m.Stats1[i] = Idle
	}
	for i := range m.Stats2 {
		m.Stats2[i] = Idle
	}
	m.server()
	return &m
}

func (m *Master) AssignTask(args *Empty, reply *TaskDescription) error {
	m.Lock()
	defer m.Unlock()
	defer log.Println("master: assign task", reply)
	for i, s := range m.Stats1 {
		if s == Idle {
			reply.Type = Task1
			reply.ID = i
			return nil
		}
	}
	for i, s := range m.Stats2 {
		if s == Idle {
			reply.Type = Task2
			reply.ID = i
			return nil
		}
	}
	reply.Type = NoTask
	m.AllWorkDone = true
	return nil
}

func (m *Master) FinishTask(args *TaskDescription, reply *Empty) error {
	m.Lock()
	defer m.Unlock()
	log.Println("master: task", args, "is finished")
	if args.Type == Task1 {
		m.Stats1[args.ID] = Done
	} else {
		m.Stats2[args.ID] = Done
	}
	return nil
}

func (m *Master) Done() bool {
	m.Lock()
	defer m.Unlock()
	return m.AllWorkDone
}
